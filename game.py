import logging
from random import randint
from sys import exit
from time import sleep

import pygame
from pygame.locals import *

from libz.utils import Sprite, Text


class Menu(object):
    logger = logging.getLogger('Menu')
    flaki = DOUBLEBUF
    screen_size = (800, 600)
    clock = pygame.time.Clock()
    FPS = 15

    def __init__(self):
        pygame.init()
        self.surface = pygame.display.set_mode(self.screen_size, self.flaki)
        self.nowa_gra_z_c = Text('comicsansms', 'Nowa gra z czlowiekiem', 22)
        self.nowa_gra_z_k = Text('comicsansms', 'Nowa gra komputerem', 22)
        self.exit_box = Text('comicsansms', 'Exit', 22)
        self.running = 1

    def run(self):
        srodek_pos = (self.screen_size[0]/2, self.screen_size[1]/2)
        while self.running:
            # self.logger.debug('Ow')
            self.clock.tick(self.FPS)
            self.surface.fill((0, 0, 0))

            self.nowa_gra_z_k.render_frame(
                self.surface,
                srodek_pos[0] - (self.nowa_gra_z_k.width/2),
                srodek_pos[1] - (self.nowa_gra_z_k.height*3),
                5
            )

            self.nowa_gra_z_c.render_frame(
                self.surface,
                srodek_pos[0] - (self.nowa_gra_z_c.width/2),
                srodek_pos[1] - self.nowa_gra_z_c.height,
                5
            )

            self.exit_box.render_frame(
                self.surface,
                srodek_pos[0] - (self.exit_box.width/2),
                srodek_pos[1] + self.exit_box.height,
                5
            )

            pygame.display.flip()

            for event in pygame.event.get():
                if event.type == QUIT:
                    self.running = 0
                    break

                elif event.type == MOUSEBUTTONDOWN:
                    pos = event.pos
                    if self.exit_box.check_if_frame_clicked(*pos):
                        self.running = 0

                    elif self.nowa_gra_z_c.check_if_frame_clicked(*pos):
                        g = Gra(self.surface)
                        czy_exit = g.run()
                        del g
                        self.logger.info('Koniec gry')
                        if czy_exit:
                            self.running = 0

                    elif self.nowa_gra_z_k.check_if_frame_clicked(*pos):
                        g = GraZAIEasy(self.surface)
                        czy_exit = g.run()
                        del g
                        self.logger.info('Koniec gry')
                        if czy_exit:
                            self.running = 0


class Gra(object):
    logger = logging.getLogger('Gra')

    def __init__(self, surface):
        self.FPS = 15
        self.clock = pygame.time.Clock()

        self.running = 1
        self.resetuj_plansze_gry()

        self.surface = surface

        self.pion = Sprite('images/pion.png', 'pion', x=337, y=127)
        self.poziom = Sprite('images/poziom.png', 'poziom', x=227, y=237)

        self.kolko = Sprite('images/kolko.png', 'kolko')
        self.krzyzyk = Sprite('images/krzyzyk.png', 'kolko')

        self.kolko_w = Sprite('images/kolko.png', 'kolko_w')
        self.kolko_w.image = pygame.transform.scale(self.kolko_w.image, (30, 30))

        self.krzyzyk_w = Sprite('images/krzyzyk.png', 'kolko_w')
        self.krzyzyk_w.image = pygame.transform.scale(self.krzyzyk_w.image, (30, 30))

        self.font = pygame.font.SysFont("comicsansms", 22)
        self.wynik_kolko_f = self.font.render('Ala ma kota', 1, (255, 255, 255))
        self.wynik_krzyzyk_f = self.font.render('a kot ma laga', 1, (255, 255, 255))
        self.menu_box = Text('comicsansms', 'Menu', 22)
        self.exit_box = Text('comicsansms', 'Exit', 22)

        self.wynik_kolko_l = 0
        self.wynik_krzyzyk_l = 0

        self.teraz = 'o'
        self.obrazek = self.kolko.image if self.teraz == 'o' else self.krzyzyk.image
        self.klikniecia = 0
        self.win = None

        p_x = 232
        p_y = 132
        t_pos = []

        for y in range(0, 3):
            t_y = p_y + (y * 115)

            for x in range(0, 3):
                t_pos.append((p_x + (x * 115), t_y))

        self.pozycje_pol = {}
        for nr, pozycja in enumerate(t_pos):
            self.pozycje_pol[nr+1] = pozycja

        del t_pos
        print self.pozycje_pol

    def run(self):
        end = False

        self.logger.info('Running')
        while self.running:
            self.surface.fill((0, 0, 0))
            self.menu_box.render_frame(self.surface, 720, 150, 5)
            self.exit_box.render_frame(self.surface, 725, 200, 5)

            self.clock.tick(self.FPS)

            self.surface.blit(self.obrazek, (0, 0))

            self.rysuj_plansze_gry()

            for event in pygame.event.get():
                if event.type == QUIT:
                    self.running = 0
                    end = True
                    break

                elif event.type == MOUSEBUTTONDOWN:
                    pos = event.pos
                    self.logger.debug(pos)

                    if not self.obsluz_event(pos):
                        if self.menu_box.check_if_frame_clicked(*pos):
                            self.running = 0

                        elif self.exit_box.check_if_frame_clicked(*pos):
                            self.running = 0
                            end = True

            self.pokaz_wyniki()
            pygame.display.flip()

            if self.win is not None:
                self.resetuj_plansze_gry()
                self.klikniecia = 0
                self.win = None
                sleep(3)

        return end

    def rysuj_plansze_gry(self):
        # pion
        self.surface.blit(self.pion.image, (self.pion.x, self.pion.y))
        self.surface.blit(self.pion.image, (self.pion.x + 115, self.pion.y))

        # poziom
        self.surface.blit(self.poziom.image, (self.poziom.x, self.poziom.y))
        self.surface.blit(self.poziom.image, (self.poziom.x, self.poziom.y + 115))

        for pole, obrazek in self.plansza_gry.iteritems():
            if obrazek is not None:
                self.surface.blit(
                    self.kolko.image if obrazek == 'o' else self.krzyzyk.image, self.pozycje_pol.get(pole)
                )

    def sprawdz_czy_pole_gry(self, pos):
        if pos[0] < 227 or pos[0] > 227 + self.poziom.width:
            self.logger.debug('Po za zakresem x: %i' % pos[0])
            return False

        if pos[1] < 127 or pos[1] > 127 + self.pion.height:
            self.logger.debug('Po za zakresem y: %i' % pos[1])
            return False

        return True

    def znajdz_odpowiednie_pole(self, pos):
        for pole, pozycja in self.pozycje_pol.iteritems():
            if pos[0] >= pozycja[0] and pos[0] <= pozycja[0] + 100:
                if pos[1] >= pozycja[1] and pos[1] <= pozycja[1] + 100:
                    return pole, pozycja

        else:
            return None, None

    def sprawdz_czy_ktos_wygral(self):
        self.logger.debug('Sprawdzam czy ktos wygral')

        pos_s = []
        pos_k = []
        winner = None

        # wiersze
        for w in (1, 4, 7):
            if self.plansza_gry.get(w, None) is not None:
                if self.plansza_gry[w] == self.plansza_gry.get(w + 1, None) == self.plansza_gry.get(w + 2, None):
                    pos_s = (self.pozycje_pol[w][0], self.pozycje_pol[w][1] + 50)
                    pos_k = (self.pozycje_pol[w+2][0] + 100, self.pozycje_pol[w+2][1] + 50)
                    winner = self.plansza_gry[w]
                    break

        # kolumny
        for k in (1, 2, 3):
            if self.plansza_gry.get(k, None) is not None:
                if self.plansza_gry[k] == self.plansza_gry.get(k + 3, None) == self.plansza_gry.get(k + 6, None):
                    pos_s = (self.pozycje_pol[k][0] + 50, self.pozycje_pol[k][1])
                    pos_k = (self.pozycje_pol[k + 6][0] + 50, self.pozycje_pol[k + 6][1] + 100)
                    winner = self.plansza_gry[k]
                    break

        # 1, 5, 9
        if self.plansza_gry.get(1, None) is not None:
            if self.plansza_gry[1] == self.plansza_gry.get(5, None) == self.plansza_gry.get(9, None):
                pos_s = self.pozycje_pol[1]
                pos_k = (self.pozycje_pol[9][0] + 100, self.pozycje_pol[9][1] + 100)
                winner = self.plansza_gry[1]

        # 7, 5, 3
        if self.plansza_gry.get(3, None) is not None:
            if self.plansza_gry[3] == self.plansza_gry.get(5, None) == self.plansza_gry.get(7, None):
                pos_s = (self.pozycje_pol[7][0], self.pozycje_pol[7][1] + 100)
                pos_k = (self.pozycje_pol[3][0] + 100, self.pozycje_pol[3][1])
                winner = self.plansza_gry[3]

        if winner is not None:
            return winner, pos_s, pos_k

        return None, None, None

    def pokaz_wyniki(self):
        self.surface.blit(self.kolko_w.image, (700, 10))
        self.surface.blit(self.krzyzyk_w.image, (750, 10))

        self.wynik_kolko_f = self.font.render(str(self.wynik_kolko_l), 1, (255, 255, 255))
        self.wynik_krzyzyk_f = self.font.render(str(self.wynik_krzyzyk_l), 1, (255, 255, 255))

        self.surface.blit(self.wynik_kolko_f, (710, 40))
        self.surface.blit(self.wynik_krzyzyk_f, (757, 40))

    def obsluz_event(self, pos):
        if self.sprawdz_czy_pole_gry(pos):
            pole, pole_pos = self.znajdz_odpowiednie_pole(pos)

            if pole is not None and self.plansza_gry.get(pole) is None:
                self.klikniecia += 1
                self.logger.info('%s zajmuje %s - %s' % (self.teraz, str(pole), str(pole_pos)))
                self.umiesc_obrazek_na_polu(pole, pole_pos)

                self.zmien_figure()

                if self.klikniecia > 4:
                    self.sprawdz_czy_koniec_rundy()
                        
            return True

    def sprawdz_czy_koniec_rundy(self):
        kto, start, koniec = self.sprawdz_czy_ktos_wygral()

        if kto is not None:
            self.logger.debug('wygraly %s, start: %s - koniec: %s' % (kto, start, koniec))
            pygame.draw.line(self.surface, (0, 255, 0), start, koniec)
            self.win = kto

            if kto == 'o':
                self.wynik_kolko_l += 1

            else:
                self.wynik_krzyzyk_l += 1

            return True

        elif self.klikniecia > 8:
            self.logger.info('Remis')
            self.resetuj_plansze_gry()
            self.klikniecia = 0
            self.win = 'remis'
            self.surface.blit(self.font.render('REMIS!', 1, (255, 255, 255)), (700, 60))

    def zmien_figure(self):
        if self.teraz == 'x':
            self.teraz = 'o'

        else:
            self.teraz = 'x'

        self.obrazek = self.kolko.image if self.teraz == 'o' else self.krzyzyk.image

    def umiesc_obrazek_na_polu(self, pole, pole_pos):
        self.surface.blit(self.obrazek, pole_pos)
        self.plansza_gry[pole] = self.teraz

    def resetuj_plansze_gry(self):
        self.plansza_gry = dict(
            [(x, None) for x in xrange(1, 10)]
        )


class GraZAIEasy(Gra):
    logger = logging.getLogger('Gra z AI Easy')

    def zmien_figure(self):
        super(GraZAIEasy, self).zmien_figure()

        if self.klikniecia > 4:
            # self.logger.debug('4 kliknecia')
            kto, start, koniec = self.sprawdz_czy_ktos_wygral()

            if kto is None:
                if self.ai_wybierz_pole():
                    super(GraZAIEasy, self).zmien_figure()

            else:
                self.logger.warning('Hell no')
        else:
            if self.ai_wybierz_pole():
                super(GraZAIEasy, self).zmien_figure()

    def znajdz_nie_zajete_pola(self):
        return [pole for pole, v in self.plansza_gry.iteritems() if v is None]

    def ai_wybierz_pole(self):
        self.logger.debug(self.plansza_gry)
        puste = self.znajdz_nie_zajete_pola()
        self.logger.debug(puste)
        if puste:
            wybrane_pole = puste[randint(0, len(puste) - 1)]
            self.logger.debug('daje %s na pole %d' % (self.teraz, wybrane_pole))
            wybrane_pole_pos = self.pozycje_pol.get(wybrane_pole)
            self.umiesc_obrazek_na_polu(wybrane_pole, wybrane_pole_pos)
            self.klikniecia += 1

            return True


if __name__ == '__main__':
    formater = '%(asctime)s %(name)8s %(levelname)6s   %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=formater)
    m = Menu()
    m.run()
    exit()