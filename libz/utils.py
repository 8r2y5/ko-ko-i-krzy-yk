#-*- coding: utf-8 -*-
import os
import logging
from functools import wraps, partial

import pygame

class Sprite(object):
    logger = logging.getLogger('Sprite')

    @property
    def image(self):
        return self.__image

    @image.setter
    def image(self, surface):
        # self.logger.debug('Nadpisuje self.__image')
        self.__image = surface
        self.width, self.height = self.__image.get_size()

    def __init__(
            self, path_to_image = None, name = None, x = 0, y = 0, speed = 1.2, collision_with_edges = True,
            screen_size = (800, 600) ):
        """
        :param path_to_image: ścieżka do pliku: string
        :param name: nazwa "obiektu": string
        :param x: pozycja x: int
        :param y: pozycja y: int
        :param speed: szybkość poruszania się: float
        :param collision_with_edges: czy obiekt wpada w kolizje z krawęzdiami ekranu: bool
        :param screen_size: wielkość ekranu: tuple(int, int)
        :return: None
        """
        if path_to_image is not None and os.path.exists(path_to_image):
            self.x = x
            self.y = y
            self.speed = speed
            self.collisions_with_edges = collision_with_edges
            self.screen_size = screen_size
            self.name = name
            self.path_to_image = path_to_image

            self.logger.debug('Loading %s' % path_to_image)
            self.image = pygame.image.load(path_to_image)


        else:
            self.logger.error('This path does not exists %s' % str(path_to_image))
            self.set_to_none()


    def set_to_none(self):
        """
        Ustawia wszystkie "ważne" wartości na None
        :return:
        """
        (self.image, self.width, self.height, self.x, self.y, self.speed,) = (None, None, None, None, None, None)


    def move(self, x, y):
        """
        Służy do przemieszczenia, gdy collisions_with_edges jest True, wykrywa kolizje z krawędziami ekranu
        :param x:
        :param y:
        :return:
        """
        if self.collisions_with_edges:
            for test in [
                            self.x + x * self.speed < self.screen_size[0],
                            self.x + x * self.speed > self.screen_size[0] - self.width,
                            self.y + y * self.speed < self.screen_size[1],
                            self.y + y * self.speed > self.screen_size[1] - self.height
            ]:
                if test:
                    self.logger.warning('Colission with the edges of the screen')
                    return 

        self.x = self.x + (x * self.speed)
        self.y = self.y + (y * self.speed)


    def get_pos(self):
        """
        :return: Zwraca krotke z pozycja (x, y)
        """
        return self.x, self.y


    def __unicode__(self):
        return u'<Sprite(path_to_image=%s, name=%s, height=%s, width=%s, x=%s, y=%s, speed=%s, collision_with_edges=%s, screen_size=%s)>' % tuple(
            map(str, [self.path_to_image, self.name, self.height, self.width, self.x, self.y, self.speed,
                      self.collisions_with_edges, self.screen_size]
            )
        )


    def __repr__(self):
        return repr(self.__unicode__())



class Text(object):
    logger = logging.getLogger('Text')

    @property
    def text(self):
        return self.__text


    @text.setter
    def text(self, val):
        self.text_msg = val
        self.__text = self.font.render(val, 1, (255,255,255))
        self.width, self.height = self.__text.get_size()


    @property
    def text_size(self):
        return self.__text_size


    @text_size.setter
    def text_size(self, val):
        self.__text_size = val
        self.font = pygame.font.SysFont(self.font_name, self.text_size)
        self.text = self.font.render(self.text_msg, 1, (255,255,255))

    @property
    def font_name(self):
        return self.__font_name


    @font_name.setter
    def font_name(self, val):
        self.__font_name = val
        self.font = pygame.font.SysFont(self.font_name, self.text_size)


    def __init__(self, font_name, text, size, x=0, y=0, frame_size=5):
        self.__text_size = size
        self.font_name = font_name
        self.text = text

        self.frame_size = frame_size
        self.frame_points = 0
        self.frame_pos = 0

        self.x, self.y = x, y


    # @staticmethod
    def check_frame_size(method):

        @wraps(method)
        def wrapper(self, x, y, frame_size=None):
            if frame_size is None:
                frame_size = self.frame_size

            if frame_size < 0:
                frame_size = 0

            out = method(self, x, y, frame_size)
            return out

        return wrapper


    def render_frame(self, surface, x, y, frame_size):
        if self.x == x and self.y == y and self.frame_pos:
            pos = self.frame_pos

        else:
            pos = self.get_frame_pos(x, y, frame_size)
            self.x, self.y = x, y
        # print pos

        pygame.draw.polygon(surface, (255, 0, 0, 200), pos)
        surface.blit(self.text, (x,y))


    @check_frame_size
    def get_frame_points(self, x, y, frame_size):
        if self.x != x and self.y != y and not self.frame_points:
            frame_x = x - frame_size
            frame_y = y - frame_size

            frame_w = self.width + frame_size + x
            frame_h = self.height + frame_size + y

            self.frame_points = frame_x, frame_y, frame_w, frame_h

        return self.frame_points


    @check_frame_size
    def get_frame_pos(self, x, y, frame_size):
        frame_x, frame_y, frame_w, frame_h = self.get_frame_points(x, y, frame_size)

        return [(frame_x, frame_y), (frame_w, frame_y), (frame_w, frame_h), (frame_x, frame_h)]

    def check_if_clicked(self, x, y):
        # self.logger.debug('%s %s %s %s' % (self.x, x, self.y, y))
        if x >= self.x and x <= self.x + self.width:
            if y >= self.y and y <= self.y + self.height:
                return True

        return False


    def check_if_frame_clicked(self, x, y):
        sx, sy, sw, sh = self.get_frame_points(x, y)
        if x >= sx and x <= sw:
            if y >= sy and y <= sh:
                return True

        return False


    def __unicode__(self):
        return u'<Text(text_msg=%s, text_size=%s, font_name=%s, height=%s, width=%s, frame_size=%s)>' % tuple(
            map(str, [self.text_msg, self.text_size, self.font_name, self.height, self.width, self.frame_size]
            )
        )


    def __repr__(self):
        return repr(self.__unicode__())